﻿using ApiService.AutoMapper.Profiles;
using ApiService.AutoMapperProfile.Profiles;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.AutoMapper
{
    public class BuildConfigAutoMapper
    {
        protected void AddProfiles(IMapperConfigurationExpression mapperConfigurationExpression)
        {
            mapperConfigurationExpression.AddProfile<UserProfile>();
            mapperConfigurationExpression.AddProfile<RoleProfile>();
            mapperConfigurationExpression.AddProfile<TaskDiaryProfile>();
        }

        public void BuilderConfig(IMapperConfigurationExpression mapperConfigurationExpression)
        {
            AddProfiles(mapperConfigurationExpression);
            
        }
    }
}
