﻿using ApiService.Data.Context;
using ApiService.Data.Dto;
using ApiService.Data.Model;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.AutoMapperProfile.Profiles
{
    public class UserProfile :Profile
    {
        public UserProfile()
        {
            CreateMap<RegisterUserDto, User>()
                .ForMember(src => src.PasswordHash, (opt)=>opt.MapFrom(src=>src.Password))
                .ReverseMap();
            CreateMap<LogginUserDto,User>().ReverseMap();
            CreateMap<User, UserBasicInfoDto>().ReverseMap();

        }
    }
}
