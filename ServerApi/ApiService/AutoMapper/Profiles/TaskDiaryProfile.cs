﻿using ApiService.Data.Dto;
using ApiService.Data.Model;
using AutoMapper;

namespace ApiService.AutoMapper.Profiles
{
    public class TaskDiaryProfile:Profile
    {
        public TaskDiaryProfile()
        {
            CreateMap<TaskDiary,TaskDiaryDto>().ReverseMap();
            CreateMap<CreateTaskDiaryDto,TaskDiary>().ReverseMap();
            CreateMap<TaskDiaryDto, CreateTaskDiaryDto>().ReverseMap();
        }
    }
}
