﻿using ApiService.Data.Dto;
using ApiService.Data.Model;
using AutoMapper;

namespace ApiService.AutoMapper.Profiles
{
    public class RoleProfile:Profile
    {
        public RoleProfile()
        {
            CreateMap<Role, RoleDto>().ReverseMap();
            
        }
    }
}
