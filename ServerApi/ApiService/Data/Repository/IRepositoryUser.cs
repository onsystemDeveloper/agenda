﻿
using ApiService.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Data.Repository
{
    public interface IRepositoryUser
    {
        public User? GetUserByIdUser(int id);
        public List<Role> GetRolesByIdUser(int id);
    }
}
