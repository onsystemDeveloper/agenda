﻿using ApiService.Data.Context;
using ApiService.Data.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication;

namespace ApiService.Data.Repository
{
    public class RepositorySession:IRepositorySession
    {
        private readonly CoreContextDB contextDB;
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;

        public RepositorySession(CoreContextDB contextDB, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            this.contextDB = contextDB;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public async Task<bool> Register(User t)
        {
            User? user = await userManager.FindByNameAsync(t.UserName);
            if (user != null)
            {
                return false;
            }

            IdentityResult result = await userManager.CreateAsync(t);
            if (!result.Succeeded)
            {
                return false;
            }

            User userRegisteredResult = await userManager.FindByNameAsync(t.UserName);
            result = await userManager.AddToRoleAsync(userRegisteredResult, PolicityValues.UserRole);
            if (!result.Succeeded)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> Loggin(string userName, string password)
        {

            User? user = await userManager.FindByNameAsync(userName);
            if (user == null)
            {
                return false;
            }
            if (user.PasswordHash != password)
            {
                return false;
            }

            await signInManager.SignInAsync(user, true);
            
            return true;

        }

        public async Task<bool> Logout()
        {
            await signInManager.SignOutAsync();
            
            return true;
        }
    }
}
