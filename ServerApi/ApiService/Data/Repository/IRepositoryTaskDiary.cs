﻿using ApiService.Data.Model;

namespace ApiService.Data.Repository
{
    public interface IRepositoryTaskDiary
    {
        public List<TaskDiary> GetTaskDiaryByUserId(int id);
        public Task<bool> CreateTaskDiary(TaskDiary taskDiary);
        public TaskDiary? GetTaskDiaryById(int id);
        public Task<bool> UpdateTaskDiary(TaskDiary taskDiary);

    }
}
