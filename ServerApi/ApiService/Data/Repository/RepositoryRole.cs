﻿using ApiService.Data.Context;
using ApiService.Data.Model;
using Microsoft.AspNetCore.Identity;

namespace ApiService.Data.Repository
{
    public class RepositoryRole:IRepositoryRole
    {
        private readonly RoleManager<Role> roleManager;
        private readonly UserManager<User> userManager;
        private readonly CoreContextDB coreContextDB;
        public RepositoryRole(RoleManager<Role> roleManager, UserManager<User> userManager,
            CoreContextDB coreContextDB)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
            this.coreContextDB = coreContextDB;
        }

        public async Task<bool> AddUserInRole(User user, string role)
        {
            IdentityResult identityResult = await userManager.AddToRoleAsync(user, role);
            if (!identityResult.Succeeded)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> CreateRole(Role role)
        {
            IdentityResult identityResult=await roleManager.CreateAsync(role);
            if (!identityResult.Succeeded)
            {
                return false;
            }

            return true;
        }

        public List<Role> GetAllRoles()
        {
            List<Role> roles = (from role in coreContextDB.Roles
                                select role).ToList();
            return roles;
        }

        public Role? GetRoleById(int id)
        {
            Role? roleSearched = (from role in coreContextDB.Roles
                         where role.Id == id
                         select role).FirstOrDefault();
            return roleSearched;
        }

        public Role? GetRoleByName(string name)
        {
            Role? roleSearched = (from role in coreContextDB.Roles
                                  where role.Name == name
                                  select role).FirstOrDefault();
            return roleSearched;
        }

        public async Task<bool> RemoveRole(Role role)
        {
            IdentityResult identityResult=await roleManager.DeleteAsync(role);
            if (!identityResult.Succeeded)
            {
                return false;
            }

            return true;
        }

        public async Task<bool> UpdateRole(Role role)
        {
            IdentityResult identityResult = await roleManager.UpdateAsync(role);
            if (!identityResult.Succeeded)
            {
                return false;
            }

            return true;
        }
    }
}
