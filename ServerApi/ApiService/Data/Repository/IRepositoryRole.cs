﻿using ApiService.Data.Model;

namespace ApiService.Data.Repository
{
    public interface IRepositoryRole
    {
        public Task<bool> CreateRole(Role role);
        public Task<bool> UpdateRole(Role role);
        public Task<bool> RemoveRole(Role role);
        public List<Role> GetAllRoles();
        public Role? GetRoleById(int id);
        public Role? GetRoleByName(string name);
        public Task<bool> AddUserInRole(User user,string role);
    }
}
