﻿using ApiService.Data.Context;
using ApiService.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace ApiService.Data.Repository
{
    public class RepositoryTaskDiary : IRepositoryTaskDiary
    {
        private readonly CoreContextDB coreContextDB;
        public RepositoryTaskDiary(CoreContextDB coreContextDB)
        {
            this.coreContextDB = coreContextDB;
        }
        public async Task<bool> CreateTaskDiary(TaskDiary taskDiary)
        {
            var entityTaskDiary=coreContextDB.Add(taskDiary);
            await coreContextDB.SaveChangesAsync();
            return true;
        }


        public TaskDiary? GetTaskDiaryById(int id)
        {
            TaskDiary? taskDiary=(from task in coreContextDB.TaskDiaries
                                 where task.Id==id
                                 select task).FirstOrDefault();
            return taskDiary;
        }

        public List<TaskDiary> GetTaskDiaryByUserId(int id)
        {
            List<TaskDiary> taskDiaryList = (from taskDiary in coreContextDB.TaskDiaries
                                             where taskDiary.UserId == id
                                             select taskDiary).ToList();
            return taskDiaryList;
        }


        public async Task<bool> UpdateTaskDiary(TaskDiary taskDiary)
        {
            var entityTaskDiary=coreContextDB.Update(taskDiary);
            await coreContextDB.SaveChangesAsync();
            return true;

        }

    }
}
