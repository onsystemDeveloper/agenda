﻿using ApiService.Data.Model;

namespace ApiService.Data.Repository
{
    public interface IRepositorySession
    {
        Task<bool> Register(User t);
        Task<bool> Loggin(string userName, string password);
        Task<bool> Logout();
    }
}
