﻿
using ApiService.Data.Context;
using ApiService.Data.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Data.Repository
{
    public class RepositoryUser : IRepositoryUser
    {
        private readonly CoreContextDB contextDB;

        public RepositoryUser(CoreContextDB contextDB)
        {
            this.contextDB = contextDB;
        }

        public User? GetUserByIdUser(int id)
        {
            User? user=contextDB.Users.FirstOrDefault(u => u.Id == id);
            
            return user;
        }

        public List<Role> GetRolesByIdUser(int id)
        {
            List<Role> searchRole = (from roleAssigned in contextDB.UserRoles
                                     join rol in contextDB.Roles on roleAssigned.RoleId equals rol.Id
                                     where roleAssigned.UserId==id
                                     select rol).ToList();          
            return searchRole;
        }

    }
}
