﻿namespace ApiService.Data
{
    public class PolicityValues
    {

        public const string RequiredUserRole = "RequiredUserRole";
        public const string UserRole = "User";

        public const string RequiredAdminRole = "RequiredAdminRole";
        public const string AdminRole = "Admin";
    }
}
