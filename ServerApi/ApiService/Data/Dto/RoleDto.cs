﻿namespace ApiService.Data.Dto
{
    public class RoleDto
    {
        public int Id { get; set; } = 0;
        public string Name { get; set; } = String.Empty;
    }
}
