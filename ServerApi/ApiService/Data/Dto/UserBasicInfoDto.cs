﻿using ApiService.Data.Model;

namespace ApiService.Data.Dto
{
    public class UserBasicInfoDto
    {
        public string Name { get; set; }=String.Empty;
        public string UserName { get; set; } = String.Empty;
        public List<RoleDto> Roles { get; set; } = new List<RoleDto>();
    }
}
