﻿namespace ApiService.Data.Dto
{
    public class TaskDiaryDto
    {
        public int Id { get; set; } = 0;
        public DateTime DateTime { get; set; }
        public string Title { get; set; } = String.Empty;
        public string Description { get; set; } = String.Empty;
    }
}
