﻿namespace ApiService.Data.Dto
{
    public class CreateTaskDiaryDto
    {
        public DateTime DateTime { get; set; }
        public string Title { get; set; } = String.Empty;
        public string Description { get; set; } = String.Empty;
    }
}
