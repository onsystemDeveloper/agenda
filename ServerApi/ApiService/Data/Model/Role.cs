﻿using Microsoft.AspNetCore.Identity;

namespace ApiService.Data.Model
{
    public class Role: IdentityRole<int>
    {
        public Role()
        {
            this.Id = 0;
        }
    }
}
