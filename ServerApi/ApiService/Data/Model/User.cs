﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiService.Data.Model
{
    
    public class User: IdentityUser<int>
    {
        public User()
        {
            Id = 0;
        }
        public override int Id { get => base.Id; set => base.Id = value; }
        public string Name { get; set; }

        public List<TaskDiary> TaskDiaries { get; set; }

    }
}
