﻿using ApiService.AutoMapper;
using ApiService.Data;
using ApiService.Data.Context;
using ApiService.Data.Model;
using ApiService.Data.Repository;
using ApiService.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;

namespace ApiService
{
    public static class ExtensionConfigurationWebApplication
    {
        public static void ConfigureDefaultIdentityService(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddDbContext<CoreContextDB>();
            serviceCollection.AddIdentity<User, Role>()
                .AddEntityFrameworkStores<CoreContextDB>();

            serviceCollection.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;
                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;
                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = false;

            });
        }
        public static async Task PreConfigureDatabase(this IApplicationBuilder app,IHostEnvironment? hostEnvironment)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>()!.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<CoreContextDB>();
                if (context!=null&&!(context.GetService<IDatabaseCreator>() as RelationalDatabaseCreator)!.Exists())
                {
                    context.Database.Migrate();
                    context.Roles.Add(new Role() { Name = PolicityValues.UserRole, NormalizedName = PolicityValues.UserRole, ConcurrencyStamp = PolicityValues.UserRole });
                    context.Roles.Add(new Role() { Name = PolicityValues.AdminRole, NormalizedName = PolicityValues.AdminRole, ConcurrencyStamp = PolicityValues.AdminRole });
                    context.SaveChanges();
                }
            }
            if (hostEnvironment != null && hostEnvironment.IsDevelopment())
            {
                using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>()!.CreateScope())
                {
                    var serviceSession=serviceScope.ServiceProvider.GetRequiredService<IServiceManagerSession>();
                    var serviceRole = serviceScope.ServiceProvider.GetRequiredService<IServiceRole>();
                    if (serviceSession!=null&&serviceRole!=null)
                    {
                        await serviceSession.Register(new Data.Dto.RegisterUserDto()
                        {
                            UserName="admin",
                            Name= "admin",
                            Email= "admin",
                            Password= "admin",
                        });
                        await serviceRole.AddUserToRole(1, PolicityValues.AdminRole);
                    }
                }
            }
        }
        public static void ConfigureAutoMapper(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddAutoMapper((cfg) =>
            {
                BuildConfigAutoMapper builderConfig = new BuildConfigAutoMapper();
                builderConfig.BuilderConfig(cfg);
            });
        }
        public static void ConfigureDefaultServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddHttpContextAccessor();
            serviceCollection.AddScoped<IRepositorySession, RepositorySession>();
            serviceCollection.AddScoped<IServiceManagerSession, ServiceManagerSession>();
            serviceCollection.AddScoped<IRepositoryUser, RepositoryUser>();
            serviceCollection.AddScoped<IServiceUser, ServiceUser>();
            serviceCollection.AddScoped<IRepositoryRole, RepositoryRole>();
            serviceCollection.AddScoped<IServiceRole, ServiceRole>();
            serviceCollection.AddScoped<IRepositoryTaskDiary, RepositoryTaskDiary>();
            serviceCollection.AddScoped<IServiceTaskDiary, ServiceTaskDiary>();

            serviceCollection.AddControllers();
        }
        public static void ConfigurePolicyRole(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddAuthorization(options =>
            {
                options.AddPolicy(PolicityValues.RequiredUserRole, policy => policy.RequireRole(PolicityValues.UserRole));
                options.AddPolicy(PolicityValues.RequiredAdminRole, policy => policy.RequireRole(PolicityValues.AdminRole));
            });
        }
        public static void ConfigureCorsPolicity(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddCors(options =>
            {
                options.AddDefaultPolicy(
                    policy =>
                    {
                        //WithOrigins("http://localhost:3000", "http://192.168.0.7:3000")
                        policy.SetIsOriginAllowed(origin => true)
                              .AllowAnyHeader()
                              .AllowAnyMethod()
                              .AllowCredentials();
                    });
            });
        }
        public static void ConfigureCookieSetting(this IServiceCollection serviceCollection)
        {
            serviceCollection.ConfigureApplicationCookie(options =>
            {
                //options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.Cookie.Name = "YourAppCookieName";
                options.Cookie.HttpOnly = false;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                options.LoginPath = "/logging";

                options.Cookie.SameSite = SameSiteMode.None;
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;

                // ReturnUrlParameter requires 
                //using Microsoft.AspNetCore.Authentication.Cookies;
                options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                options.SlidingExpiration = true;
            });
        }
        public static void ConfigureToRun(this WebApplication webApplication)
        {
            webApplication.UseCors();

            if (webApplication.Environment.IsDevelopment())
            {

                webApplication.UseSwagger();
                webApplication.UseSwaggerUI((options) =>
                {
                    options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
                    options.RoutePrefix = String.Empty;
                });
            }
            // Configure the HTTP request pipeline.
            if (!webApplication.Environment.IsDevelopment())
            {

                webApplication.UseExceptionHandler("/Error");
                webApplication.UseHsts();
            }

            webApplication.UseHttpsRedirection();
            webApplication.UseStaticFiles();

            webApplication.UseAuthentication();
            webApplication.UseAuthorization();

            webApplication.MapControllers();
        }
    }
}
