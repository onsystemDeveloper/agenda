﻿using ApiService.Data;
using ApiService.Data.Dto;
using ApiService.Data.Model;
using ApiService.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ApiService.Controllers
{
    [ApiController]
    public class ControllerSession:Controller
    {
        private readonly IServiceManagerSession ServiceManagerSession;
        /// <summary>
        /// First try
        /// </summary>
        /// <param name="serviceManagerSession"></param>
        public ControllerSession(IServiceManagerSession serviceManagerSession)
        {
            this.ServiceManagerSession = serviceManagerSession;
        }
        /// <summary>
        /// User Authentication
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///    POST /logging
        ///
        /// </remarks>
        /// <param name="logginUser">Description here</param>
        /// <returns>cookie in browser</returns>
        /// <response code="200">Succesful</response>
        /// <response code="400">Error comunicate in xxx-xx-xx-xx</response>
        /// <response code="404">User not found</response>
        [HttpPost("/logging")]
        public async Task<IActionResult> Loggin([FromBody] LogginUserDto logginUser)
        {
            try
            {
                bool isAuthenticate = await ServiceManagerSession.LogginUser(logginUser);

                if (!isAuthenticate)
                {
                    return NotFound();
                }
                return Ok();
            }catch(Exception ex)
            {
                return BadRequest();
            }
        }
        /// <summary>
        /// User Register
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///    POST /register
        ///
        /// </remarks>
        /// <param name="registerUser">Description here</param>
        /// <response code="200">Succesful</response>
        /// <response code="400">Error cant register User</response>
        [HttpPost("/register")]
        public async Task<IActionResult> Register([FromBody] RegisterUserDto registerUser)
        {
            try
            {
                bool isRegister = await ServiceManagerSession.Register(registerUser);
                if (!isRegister)
                {
                    return BadRequest();
                }

                return Ok();
            }catch(Exception ex)
            {
                return BadRequest();
            }
        }
        /// <summary>
        /// User logout
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///    Get /logout
        ///
        /// </remarks>
        /// <returns>Description here</returns>
        /// <response code="200">Succesful</response>
        /// <response code="400">Cant logout try logging to use endpoint</response>
        [Authorize(PolicityValues.RequiredUserRole)]
        [HttpGet("/logout")]
        public async Task<IActionResult> LogOut()
        {
            try
            {
                bool isLogOut = await ServiceManagerSession.LogOut();
                if (!isLogOut)
                {
                    return BadRequest();
                }

                return Ok();
            }catch(Exception ex)
            {
                return BadRequest();
            }
        }
        /// <summary>
        /// Compare is Logging or not logging
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///    POST /isAuthenticate
        ///
        /// </remarks>
        /// <returns>Description here</returns>
        /// <response code="200">Succesful</response>
        /// <response code="400">not logging in session</response>
        [Authorize(PolicityValues.RequiredUserRole)]
        [HttpGet("/isAuthenticate")]
        public IActionResult IsUserAuthenticate()
        {
            try
            {
                return Ok();
            }catch(Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
