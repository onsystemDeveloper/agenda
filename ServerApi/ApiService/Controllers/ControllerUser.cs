﻿using ApiService.Data.Dto;
using ApiService.Services;
using Microsoft.AspNetCore.Mvc;

namespace ApiService.Controllers
{
    [ApiController]
    public class ControllerUser:Controller
    {

        private readonly IServiceUser ServiceUser;
        
        public ControllerUser(IServiceUser serviceUser)
        {
            this.ServiceUser = serviceUser;
        }

        [HttpGet("/userBasicInfo")]
        public IActionResult userBasicInfo()
        {
            try
            {
                UserBasicInfoDto? dto = ServiceUser.GetUserBasicInfo();
                if (dto==null)
                {
                    return BadRequest();
                }

                return Ok(dto);

            }catch(Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
