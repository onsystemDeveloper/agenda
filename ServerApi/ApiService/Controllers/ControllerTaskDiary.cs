﻿using ApiService.Data.Context;
using ApiService.Data.Dto;
using ApiService.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiService.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ControllerTaskDiary : ControllerBase
    {
        private readonly IServiceTaskDiary serviceTaskDiary;

        public ControllerTaskDiary(IServiceTaskDiary serviceTaskDiary)
        {
            this.serviceTaskDiary = serviceTaskDiary;
        }

        [HttpGet("/getTaskDiary")]
        public IActionResult getTaskDiary()
        {
            try
            {
                List<TaskDiaryDto> taskDiaryDtos = serviceTaskDiary.GetTaskDiaryUser();
                return Ok(taskDiaryDtos);
            }catch(Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPost("/createTask")]
        public async Task<IActionResult> createTaskDiary([FromBody] CreateTaskDiaryDto taskDiaryDto)
        {
            try
            {
                bool isCreated=await serviceTaskDiary.CreateTaskDiary(taskDiaryDto);
                if (!isCreated)
                {
                    return BadRequest();
                }

                return Ok(taskDiaryDto);
            }catch(Exception ex)
            {
                return BadRequest();
            }
        }

        [HttpPut("/updateTask")]
        public async Task<IActionResult> updateTaskDiary([FromBody] TaskDiaryDto taskDiaryDto)
        {
            try
            {
                bool isUpdated=await serviceTaskDiary.UpdateTaskDiary(taskDiaryDto);
                if (!isUpdated)
                {
                    return BadRequest();
                }

                return Ok();
            }catch(Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
