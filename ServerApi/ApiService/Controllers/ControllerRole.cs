﻿using ApiService.Data;
using ApiService.Data.Dto;
using ApiService.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ControllerRole : ControllerBase
    {
        private readonly IServiceRole serviceRole;
        public ControllerRole(IServiceRole serviceRole)
        {
            this.serviceRole = serviceRole;
        }

        [Authorize(PolicityValues.RequiredAdminRole)]
        [HttpGet("/getRoles")]
        public IActionResult GetRoles()
        {
            try
            {
                List<RoleDto> roleDtos=serviceRole.GetAllRoles();
                return Ok(roleDtos);
            }catch(Exception ex)
            {
                return BadRequest();
            }
        }

        [Authorize(PolicityValues.RequiredAdminRole)]
        [HttpPost("/addUserRol")]
        public async Task<IActionResult> AddUserToRol([FromQuery] int id,string role)
        {
            try
            {
                bool isAdded= await serviceRole.AddUserToRole(id,role);
                if (!isAdded)
                {
                    return NotFound();
                }

                return Ok();
            }
            catch(Exception ex)
            {
                return BadRequest();
            }
        }
        
    }
}
