﻿using System.Security.Claims;

namespace ApiService.Services.Utils
{
    public class UtilManagerHttpContext
    {
        public static int GetClaimId(HttpContext httpContext)
        {
            string id = httpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            int idParsed = int.Parse(id);
            return idParsed;
        }
    }
}
