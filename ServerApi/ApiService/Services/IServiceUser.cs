﻿using ApiService.Data.Dto;

namespace ApiService.Services
{
    public interface IServiceUser
    {
        public UserBasicInfoDto? GetUserBasicInfo();
    }
}
