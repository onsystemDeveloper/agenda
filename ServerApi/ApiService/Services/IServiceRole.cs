﻿using ApiService.Data.Dto;
using ApiService.Data.Model;

namespace ApiService.Services
{
    public interface IServiceRole
    {
        public Task<bool> CreateRole(RoleDto role);
        public Task<bool> UpdateRole(RoleDto role);
        public Task<bool> RemoveRole(RoleDto role);
        public List<RoleDto> GetAllRoles();
        public RoleDto? GetRoleById(int id);
        public RoleDto? GetRoleByName(string name);
        public Task<bool> AddUserToRole(int userId, string role);
    }
}
