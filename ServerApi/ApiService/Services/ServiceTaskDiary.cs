﻿using ApiService.Data.Dto;
using ApiService.Data.Model;
using ApiService.Data.Repository;
using ApiService.Services.Utils;
using AutoMapper;

namespace ApiService.Services
{
    public class ServiceTaskDiary : IServiceTaskDiary
    {
        private readonly IMapper mapper;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IRepositoryTaskDiary repositoryTaskDiary;
        public ServiceTaskDiary(IMapper mapper, IHttpContextAccessor httpContextAccessor,
            IRepositoryTaskDiary repositoryTaskDiary)
        {
            this.mapper = mapper;
            this.httpContextAccessor = httpContextAccessor;
            this.repositoryTaskDiary = repositoryTaskDiary;
        }
        public async Task<bool> CreateTaskDiary(CreateTaskDiaryDto taskDiary)
        {
            try
            {
                TaskDiary task=mapper.Map<TaskDiary>(taskDiary);

                if (httpContextAccessor.HttpContext==null)
                {
                    return false;
                }
                int idUser=UtilManagerHttpContext.GetClaimId(httpContextAccessor.HttpContext);
                task.UserId=idUser;

                bool isCreated = await repositoryTaskDiary.CreateTaskDiary(task);
                return isCreated;
                
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<TaskDiaryDto> GetTaskDiaryByUserId(int id)
        {
            try
            {
                List<TaskDiary> taskDiaries = repositoryTaskDiary.GetTaskDiaryByUserId(id);
                List<TaskDiaryDto> taskDiariesDto = mapper.Map<List<TaskDiaryDto>>(taskDiaries);
                return taskDiariesDto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TaskDiaryDto> GetTaskDiaryUser()
        {
            if (httpContextAccessor.HttpContext==null)
            {
                return new List<TaskDiaryDto>();
            }
            
            int id = UtilManagerHttpContext.GetClaimId(httpContextAccessor.HttpContext);
            List<TaskDiaryDto> taskDiaryDtos = GetTaskDiaryByUserId(id);
            return taskDiaryDtos;
        }

        public async Task<bool> UpdateTaskDiary(TaskDiaryDto taskDiary)
        {
            try
            {
                if (httpContextAccessor.HttpContext==null)
                {
                    return false;
                }

                TaskDiary task = mapper.Map<TaskDiary>(taskDiary);
                int idUser=UtilManagerHttpContext.GetClaimId(httpContextAccessor.HttpContext);
                task.UserId=idUser;

                bool isUpdated = await repositoryTaskDiary.UpdateTaskDiary(task);
                return isUpdated;
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}
