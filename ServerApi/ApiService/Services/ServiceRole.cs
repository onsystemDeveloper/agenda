﻿using ApiService.Data;
using ApiService.Data.Dto;
using ApiService.Data.Model;
using ApiService.Data.Repository;
using ApiService.Services.Utils;
using AutoMapper;

namespace ApiService.Services
{
    public class ServiceRole : IServiceRole
    {
        private readonly IMapper mapper;
        private readonly IRepositoryRole repositoryRole;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IRepositoryUser repositoryUser;
        public ServiceRole(IRepositoryRole repositoryRole,IRepositoryUser repositoryUser,
            IHttpContextAccessor httpContextAccessor, IMapper mapper)
        {
            this.mapper = mapper;
            this.repositoryRole = repositoryRole;
            this.repositoryUser = repositoryUser;
            this.httpContextAccessor = httpContextAccessor;
        }

        public async Task<bool> AddUserToRole(int userId, string role)
        {
            try
            {
                User? user=repositoryUser.GetUserByIdUser(userId);
                if (user==null)
                {
                    return false;
                }
                bool isAddedRole=await repositoryRole.AddUserInRole(user,role);
                return isAddedRole;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> CreateRole(RoleDto role)
        {
            try
            {
                Role roleModel=mapper.Map<Role>(role);
                if (repositoryRole.GetRoleByName(roleModel.Name)!=null)
                {
                    return false;
                }

                bool isCreated = await repositoryRole.CreateRole(roleModel);
                return isCreated;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<RoleDto> GetAllRoles()
        {
            try
            {
                List<Role> roles = repositoryRole.GetAllRoles();
                List<RoleDto> rolesDto = mapper.Map<List<RoleDto>>(roles);
                return rolesDto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public RoleDto? GetRoleById(int id)
        {
            try
            {
                Role? role = repositoryRole.GetRoleById(id);
                if (role == null)
                {
                    return null;
                }

                RoleDto roleDto = mapper.Map<RoleDto>(role);
                return roleDto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public RoleDto? GetRoleByName(string name)
        {
            try
            {
                Role? role = repositoryRole.GetRoleByName(name);
                if (role==null)
                {
                    return null;
                }

                RoleDto roleDto = mapper.Map<RoleDto>(name);
                return roleDto;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<bool> RemoveRole(RoleDto role)
        {
            try
            {
                Role roleModel=mapper.Map<Role>(role);
                bool isCreated = await repositoryRole.RemoveRole(roleModel);
                return isCreated;
            }
            catch (Exception)
            {
                throw; 
            }
        }

        public async Task<bool> UpdateRole(RoleDto role)
        {
            try
            {
                Role roleModel = mapper.Map<Role>(role);
                bool isUpdate=await repositoryRole.UpdateRole(roleModel);
                return isUpdate;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
