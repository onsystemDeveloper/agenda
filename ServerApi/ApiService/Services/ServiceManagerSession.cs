﻿using ApiService.Data.Context;
using ApiService.Data.Dto;
using ApiService.Data.Model;
using ApiService.Data.Repository;
using AutoMapper;
using Microsoft.AspNetCore.Identity;

namespace ApiService.Services
{
    public class ServiceManagerSession : IServiceManagerSession
    {
        private readonly IMapper mapper;
        private readonly IRepositorySession sessionRepository;
        private readonly IHttpContextAccessor httpContextAccessor;
        public ServiceManagerSession(IMapper mapper, IRepositorySession sessionRepository, IHttpContextAccessor httpContextAccessor)
        {
            this.mapper = mapper;
            this.sessionRepository = sessionRepository;
            this.httpContextAccessor = httpContextAccessor;
        }

        public async Task<bool> LogginUser(LogginUserDto logginUser)
        {
            try
            {
                bool isLoggin = await sessionRepository.Loggin(logginUser.UserName, logginUser.Password);
                if (!isLoggin)
                {
                    return false;
                }

                return true;
            }catch(Exception)
            {
                throw;
            }
        }

        public async Task<bool> Register(RegisterUserDto registerUser)
        {
            try
            {
                User userRegister = mapper.Map<User>(registerUser);
                if (userRegister == null)
                {
                    return false;
                }

                bool isRegister = await sessionRepository.Register(userRegister);
                return isRegister;
            }catch(Exception)
            {
                throw;
            }
        }
        public async Task<bool> LogOut()
        {

            try
            {
                return await sessionRepository.Logout();
            }catch(Exception)
            {
                throw;
            }
        }


    }
}
