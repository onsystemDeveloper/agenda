﻿using ApiService.Data.Dto;
using ApiService.Data.Model;
using ApiService.Data.Repository;
using ApiService.Services.Utils;
using AutoMapper;
using System.Security.Claims;

namespace ApiService.Services
{
    public class ServiceUser : IServiceUser
    {
        private readonly IMapper mapper;
        private readonly IRepositoryUser userRepository;
        private readonly IHttpContextAccessor httpContextAccessor;
        public ServiceUser(IMapper mapper, IRepositoryUser userRepository, IHttpContextAccessor httpContextAccessor)
        {
            this.userRepository = userRepository;
            this.mapper = mapper;
            this.httpContextAccessor = httpContextAccessor;
        }

        public UserBasicInfoDto? GetUserBasicInfo()
        {
            try
            {
                if (this.httpContextAccessor.HttpContext == null)
                {
                    return null;
                }
                int idParsed = UtilManagerHttpContext.GetClaimId(this.httpContextAccessor.HttpContext);

                User? user = userRepository.GetUserByIdUser(idParsed);
                if (user == null)
                {
                    return null;
                }
                List<Role> rolesUser = userRepository.GetRolesByIdUser(idParsed);
                List<RoleDto> rolesDto = mapper.Map<List<RoleDto>>(rolesUser);

                UserBasicInfoDto userBasicInfo = mapper.Map<UserBasicInfoDto>(user);
                userBasicInfo.Roles = rolesDto;

                return userBasicInfo;
            }catch(Exception)
            {
                throw;
            }
        }

    }
}
