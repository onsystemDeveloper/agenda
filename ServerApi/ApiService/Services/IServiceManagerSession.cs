﻿using ApiService.Data.Dto;

namespace ApiService.Services
{
    public interface IServiceManagerSession
    {
        Task<bool> LogginUser(LogginUserDto logginUser);
        Task<bool> Register(RegisterUserDto registerUser);
        Task<bool> LogOut();

    }
}
