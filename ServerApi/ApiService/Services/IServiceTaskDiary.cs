﻿using ApiService.Data.Dto;
using ApiService.Data.Model;

namespace ApiService.Services
{
    public interface IServiceTaskDiary
    {
        public List<TaskDiaryDto> GetTaskDiaryByUserId(int id);
        public List<TaskDiaryDto> GetTaskDiaryUser();
        public Task<bool> CreateTaskDiary(CreateTaskDiaryDto taskDiary);
        public Task<bool> UpdateTaskDiary(TaskDiaryDto taskDiary);

    }
}
