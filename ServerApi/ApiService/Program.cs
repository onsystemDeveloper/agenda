
using ApiService;
using ApiService.AutoMapper;
using ApiService.Data;
using ApiService.Data.Context;
using ApiService.Data.Model;
using ApiService.Data.Repository;
using ApiService.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Reflection;

public partial class Program
{
    public static async Task Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Configuration.SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json").Build();

        builder.Services.ConfigureCorsPolicity();
        builder.Services.ConfigureCookieSetting();
        builder.Services.ConfigureDefaultIdentityService();
        builder.Services.ConfigureAutoMapper();
        builder.Services.ConfigurePolicyRole();
        builder.Services.ConfigureDefaultServices();

        if (builder.Environment.IsDevelopment())
        {
            builder.Services.AddSwaggerGen(c =>
            {
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

        }

        var app = builder.Build();
        await app.PreConfigureDatabase(builder.Environment);
        app.ConfigureToRun();
        app.Run();
    }
}