import i18n from 'i18next'
import {initReactI18next} from 'react-i18next'
import translation_EN from'./en/translation_EN.json'
import translation_ES from './es/translation_ES.json'


const resources={
    es:{
        translation: translation_ES
    },
    en:{
        translation:translation_EN
    }
}

i18n
    .use(initReactI18next)
    .init({
        resources,
        lng:"es",
        keySeparator:false,
        interpolation:{
            escapeValue:false
        }
    })

export default i18n