import { PathApiServer } from "./basePath";

export const PathLogging=PathApiServer+"logging"
export const PathRegister=PathApiServer+"register"
export const PathLogOut=PathApiServer+"logout"
export const PathIsAuthenticate=PathApiServer+"isAuthenticate"

export const PathGetTaskDiary=PathApiServer+"getTaskDiary"
export const PathCreateTask=PathApiServer+"createTask"
export const PathUpdateTask=PathApiServer+"updateTask"