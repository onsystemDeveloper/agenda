import React, { useCallback } from "react";
import { Navigate, Route, Routes } from "react-router";
import Home from "./pages/home/home";
import { useRootSelector } from "./redux/store";
import { PathLocalDiary, PathLocalHome, PathLocalLoggingOrRegister } from "./routes/local/localPaths";
import LayoutPrincipalMain from "./components/layout/layoutPrincipalMain";
import LoggingOrRegister from "./pages/loggingOrRegister/loggingOrRegister";
import TaskDiary from "./pages/diary/taskDiary";


const App:React.FC<{}>= ()=>{
    return (
        <LayoutPrincipalMain children={
            <Routes>
                <Route path={PathLocalLoggingOrRegister} element={<LoggingOrRegister/>}/>
                <Route path={PathLocalHome} element={
                    <PrivateRoute element={<Home/>}/>
                }/>
                <Route path={PathLocalDiary} element={
                    <PrivateRoute element={<TaskDiary/>}/>
                }/>
            </Routes>
        } />
    )
}



const PrivateRoute:React.FC<{element:JSX.Element,rolRequired?:string[]}>=({element,rolRequired})=>{
    const isAuthentication=useRootSelector(state=>state.user.isAuthenticate)
    
    const canNavigate=useCallback(()=>{
        return isAuthentication
    },[isAuthentication])

    if(!canNavigate()){
        return (
            <Navigate to={PathLocalLoggingOrRegister}/>
        )
    }

    return element
}

export default App