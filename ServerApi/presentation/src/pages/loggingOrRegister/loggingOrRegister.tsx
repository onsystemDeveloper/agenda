import { Grid, Paper, Tab, Tabs } from "@material-ui/core";
import React, { useState } from "react";
import Logging from "../../components/loging/loggin";
import Register from "../../components/register/register";
import TabPanel from "../../components/share/tabpanel/tabpanel";


const LoggingOrRegister:React.FC<{}>=()=>{
    const [valueIndex,setValueIndex]=useState(0);

    const handleChange=(newValue: number) => {
        setValueIndex(newValue);
    };

    return (
        <div style={{
            height:"90%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
        }}>
            <Paper style={{
            minWidth:"40px",
            maxWidth:"400px",
            minHeight:"80px",
            maxHeight:"500px",
            padding:"40px",
            }}>
                <Grid container spacing={7}>
                    <Grid container item justifyContent="center">
                        <Tabs value={valueIndex}
                        onChange={(reactChangeEvent,value)=>{
                            handleChange(value)
                        }}
                        >
                            <Tab label="Logging"/>
                            <Tab label="Register" />
                        </Tabs>
                    </Grid>
                    <Grid item container justifyContent="center">
                        <TabPanel index={0} value={valueIndex}>
                            <Logging/>
                        </TabPanel>
                        <TabPanel index={1} value={valueIndex}>
                            <Register/>
                        </TabPanel>
                    </Grid>
                </Grid>
            </Paper>
        </div>
        
        
    )
}

export default LoggingOrRegister