import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Grid } from '@material-ui/core';
import TextField from '@mui/material/TextField';
import { DatePicker } from "@mui/x-date-pickers"
import dayjs from 'dayjs';
import React, { useState,useEffect } from "react"
import { useSelector } from 'react-redux';
import CardTask from '../../components/cardTask/cardTask';
import { useAppDispatch } from '../../redux/store';
import { createTask, getTask } from '../../redux/user/actionTask';
import { selectorTaskState } from '../../redux/user/taskStore';

const DialogToCreateTask:React.FC<{
    open:boolean,
    onClose:()=>void,
}>=({open,onClose})=>{
    const [stateTask,setStateTask]=useState<{
        dateTime:string,
        title:string,
        description:string
    }>({
        dateTime:"",
        description:"",
        title:""
    })

    const dispatch=useAppDispatch()

    return (
        <Dialog open={open} onClose={onClose}>
            <DialogTitle>Create Task</DialogTitle>
            <DialogContent>
                <TextField value={stateTask.title} label={"Titulo"} placeholder={"Enter Titulo"} 
                    onChange={(e)=>{setStateTask({...stateTask,title:e.target.value})}}/>
                <TextField value={stateTask.description} label={"Description"} placeholder={"Enter Description"} 
                    onChange={(e)=>{setStateTask({...stateTask,description:e.target.value})}}/>
                <DatePicker
                    mask="____/__/__"
                    value={stateTask.dateTime}
                    onChange={(newValue) => newValue&&setStateTask({...stateTask,dateTime:newValue})}
                    renderInput={(params) => <TextField {...params}/>}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={e=>{
                    setStateTask({dateTime:"",description:"",title:""})
                    onClose()
                }}>Cancel</Button>
                <Button onClick={e=>{
                    dispatch(createTask(stateTask)).then(e=>{
                        dispatch(getTask())
                    })
                    onClose()
                }}>Create Task</Button>
            </DialogActions>
        </Dialog>
    )
}


const TaskDiary:React.FC<{}>=()=>{
    const [date,setDate]=useState<number>(Date.now())
    const [disableCalendar,setDisableCalendar]=useState(false)
    const [stateDialogCreate,setStateDialogCreate]=useState(false)

    const tasksSelector=useSelector(selectorTaskState);
    const dispatch=useAppDispatch()
    
    useEffect(()=>{
        dispatch(getTask())
        return (()=>{})
    },[dispatch])
    return (
        <div>
            <Grid container style={{
                display:"block",
                height:"100%"
            }}>
                <Grid container item xs={12} style={{
                    height:"10%"
                }}>
                    <Grid item xs={9}>
                        <Button onClick={e=>{setStateDialogCreate(true)}}>New Task</Button>
                    </Grid>
                    <Grid container item xs={3} direction='row'>
                        <Grid item>
                            <Button onClick={(e)=>{
                                setDisableCalendar(!disableCalendar)
                            }}>Enable/Disabled</Button>
                        </Grid>
                        <Grid item>
                            <DatePicker
                                disabled={disableCalendar}
                                mask="____/__/__"
                                value={date}
                                onChange={(newValue) => newValue&&setDate(newValue)}
                                renderInput={(params) => <TextField {...params}/>}
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container item xs={12}>
                    {
                        !disableCalendar&&
                        tasksSelector.tasksUser.filter(e=>dayjs(e.dateTime,'YYYY/MM/DD').date()===dayjs(date,'DD/MM/YYYY').date())
                            .map(e=><CardTask taskModel={e}/>)
                    }
                    {
                        disableCalendar&&
                        tasksSelector.tasksUser.map(e=><CardTask taskModel={e}/>)
                    }
                </Grid>
            </Grid>
            <DialogToCreateTask open={stateDialogCreate} onClose={()=>{setStateDialogCreate(false)}}/>
        </div>
    )
}

export default TaskDiary