import React from 'react';
import ReactDOM from 'react-dom/client';
import { I18nextProvider } from 'react-i18next';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import App from './app';
import i18n from './i18n/i18n';
import { store } from './redux/store';
import reportWebVitals from './reportWebVitals';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import './index.css'
import { LocalizationProvider } from '@mui/x-date-pickers';


const storeApp = store

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
document.body.className="app-container"

root.render(

  <BrowserRouter>
    <I18nextProvider i18n={i18n}>
      <Provider store={storeApp}>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <App />
        </LocalizationProvider>
      </Provider>
    </I18nextProvider>
  </BrowserRouter>

);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
