import { configureStore, Action, ThunkAction } from "@reduxjs/toolkit"
import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import { taskReducer } from "./user/taskStore";
import { userReducer } from "./user/userStore";




export const store = configureStore({
    reducer: {
        user: userReducer,
        tasks:taskReducer
    },
})

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType, RootState, 
    unknown, Action<string>
>

export const useAppDispatch:()=>AppDispatch=useDispatch
export const useRootSelector:TypedUseSelectorHook<RootState>=useSelector
