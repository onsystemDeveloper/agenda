import { createAsyncThunk } from "@reduxjs/toolkit"
import TaskDiary from "../services/taskDiary"


export const createTask=createAsyncThunk('task/create',async (task:{
    dateTime:string,
    title:string,
    description:string
},thunkApi)=>{
    return await TaskDiary.createTaskDiary(task).then(e=>e.data)
})

export const updateTask=createAsyncThunk('task/update',async(task:{
    id:number,
    dateTime:string,
    title:string,
    description:string
},thunkApi)=>{
    return await TaskDiary.updateTaskDiary(task).then(e=>task)
})

export const getTask=createAsyncThunk('task/getall',async(thunkApi)=>{
    return await TaskDiary.getTaskDiary().then(e=>e.data)
})
