import { AxiosResponse } from "axios";


export const controllerAxiosResponse=(response:AxiosResponse<any,any>)=>{
    if (response.status !== 200) {
        return Promise.reject()
    }
    return Promise.resolve()
}