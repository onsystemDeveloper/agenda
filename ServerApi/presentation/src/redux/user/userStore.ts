import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { RootState } from "../store"
import { loggingUser } from "./actionAuthentication"

export type UserStoreState={
    isAuthenticate:boolean
    name:string
}

const initialUserState:UserStoreState={
    isAuthenticate:false,
    name:""
}

export const userStateSlice=createSlice({
    name:'user',
    initialState:initialUserState,
    reducers:{
        prueba:(state:UserStoreState,action:PayloadAction<string,string>)=>{
            state.name=action.payload
        }
    },
    extraReducers(builder) {
        builder.addCase(loggingUser.fulfilled,(state,action)=>{
            state.isAuthenticate=true
        })
        builder.addCase(loggingUser.rejected,(state,action)=>{
            state.isAuthenticate=false;
        })
    },
})

export const {prueba} = userStateSlice.actions
export const selectUserState=(state:RootState)=>state.user
export const userReducer= userStateSlice.reducer