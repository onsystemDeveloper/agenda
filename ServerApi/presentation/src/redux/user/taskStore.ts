import { createSlice } from "@reduxjs/toolkit"
import { RootState } from "../store"
import { createTask, getTask, updateTask } from "./actionTask"

export type TaskModel={
    id:number,
    dateTime:string,
    title:string,
    description:string
}

export type TaskStoreState={
    tasksUser:TaskModel[]
}

const initialTaskState:TaskStoreState={
    tasksUser:[]
}


export const taskStateSlice=createSlice({
    name:'task',
    initialState:initialTaskState,
    reducers:{

    },
    extraReducers(builder){
        builder.addCase(getTask.fulfilled,(state,action)=>{
            state.tasksUser=action.payload
        })

        builder.addCase(createTask.fulfilled,(state,action)=>{
            //state.tasksUser=[...state.tasksUser,action.payload.data]
        })

        builder.addCase(updateTask.fulfilled,(state,action)=>{
            let tasks=state.tasksUser
            let index:number=tasks.findIndex(e=>e.id===action.payload.id)
            tasks[index]=action.payload
            state.tasksUser=tasks
        })
    }
})


export const selectorTaskState=(state:RootState)=>state.tasks
export const taskReducer=taskStateSlice.reducer