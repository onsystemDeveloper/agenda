import { createAsyncThunk } from "@reduxjs/toolkit";
import Authenticate from "../services/authenticate";
import { controllerAxiosResponse } from "./baseControllerAction";



export const loggingUser = createAsyncThunk('user/logging',
    async (logging: {
        userName: string,
        password: string
    }, thunkApi) => {
        let response=await Authenticate.Logging(logging)
        return controllerAxiosResponse(response)
    }
)

export const registerUser=createAsyncThunk('user/register',
    async(register:{
        name:string,
        userName:string,
        email:string,
        password:string
    },thunkApi)=>{
        let response=await Authenticate.Register(register)
        return controllerAxiosResponse(response) 
    }
)

