import axios from "axios"
import { PathCreateTask, PathGetTaskDiary, PathUpdateTask } from "../../routes/api/authenticationRoutes"

class TaskDiary{
    static async getTaskDiary(){
        return await axios.get(PathGetTaskDiary,{
            withCredentials:true
        })
    }

    static async createTaskDiary(task:{
        dateTime:string,
        title:string,
        description:string
    }){
        return await axios.post(PathCreateTask,task,{
            withCredentials:true
        })
    }

    static async updateTaskDiary(task:{
        id: number,
        dateTime:string,
        title: string,
        description: string
    }){
        return await axios.put(PathUpdateTask,task,{
            withCredentials:true
        })
    }
}

export default TaskDiary