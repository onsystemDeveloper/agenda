import { PathIsAuthenticate, PathLogging, PathRegister } from '../../routes/api/authenticationRoutes';
import axios, { AxiosResponse } from "axios"
import { PathLogOut } from "../../routes/api/authenticationRoutes"



class Authenticate{

    static async LogOut():Promise<AxiosResponse<any,any>>{
        return await axios.get(PathLogOut,{
            withCredentials:true
        })
    }

    static async Register(user:{
        name:string,
        userName:string,
        email:string,
        password:string
    }):Promise<AxiosResponse<any,any>>{
        return await axios.post(PathRegister,user)
    }

    static async Logging(user:{
        userName:string,
        password:string
    }):Promise<AxiosResponse<any,any>>{
        return await axios.post(PathLogging,user,{
            withCredentials:true,
        })
    }

    static async IsAuthenticate():Promise<AxiosResponse<any,any>>{
        return await axios.get(PathIsAuthenticate,{
            withCredentials:true
        })
    }
}

export default Authenticate