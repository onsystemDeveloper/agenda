import { Button } from "@material-ui/core"
import React, { useCallback } from "react"
import { useNavigate } from "react-router"
import Authenticate from "../../redux/services/authenticate"
import { useRootSelector } from "../../redux/store"
import { PathLocalDiary,PathLocalHome, PathLocalLoggingOrRegister } from "../../routes/local/localPaths"
import './layoutPrincipalMain.css'

const ButtonsIfAuthenticate:React.FC<{
    action:(path:string)=>void
}> = ({ action }) => {
    return (
        <>
        
            <Button onClick={()=>{action(PathLocalHome)}}>Home</Button>
            <Button onClick={()=>{action(PathLocalDiary)}}>Diary</Button>
            <Button onClick={()=>{Authenticate.LogOut().then(e=>{
                action(PathLocalLoggingOrRegister)
            })}}>LogOut</Button>
        </>
    )
}

const NavigationBar: React.FC<{}> = () => {

    const navigator=useNavigate()
    const isAuthenticate=useRootSelector(state=>state.user.isAuthenticate)
    
    const funcToNavigate=useCallback((pathNavigate:string)=>{
        navigator(pathNavigate)
    },[navigator])

    return (
        <div>
            {
                isAuthenticate&&
                <ButtonsIfAuthenticate action={funcToNavigate}/>
            }
        </div>
    )
}



const LayoutPrincipalMain: React.FC<{
    children: JSX.Element
}> = ({
    children
}) => {

        return (
            <>
                <div className="container-layout-navbar">
                    <NavigationBar />
                </div>                
                <div className="container-layout-children">
                    {children}
                </div>

            </>
        )
    }

export default LayoutPrincipalMain