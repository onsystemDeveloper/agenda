import { Button, Card, CardActions, CardContent, Grid, TextField, Typography } from "@material-ui/core";
import TextFieldToDatePicker from '@mui/material/TextField';
import { DatePicker } from "@mui/x-date-pickers";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useAppDispatch } from "../../redux/store";
import { updateTask } from "../../redux/user/actionTask";
import { TaskModel } from "../../redux/user/taskStore";



const ComponentShowOrWrite:React.FC<{
    canWrite:boolean,
    label:string,
    value:string,
    updateAction:(text:string)=>void,
    placeholder:string,
}>=({canWrite,label,placeholder,value,updateAction})=>{

    return (
        <>
        {
            canWrite===false&&
            <Typography>{value}</Typography>
        }
        {
            canWrite===true&&
            <TextField label={label} placeholder={placeholder} value={value}
                    fullWidth required onChange={(e) => { updateAction(e.target.value)}} />
        }
        </>
    )
}



const CardTask:React.FC<{
    taskModel:TaskModel,
}>=({taskModel})=>{
    const {t}=useTranslation()
    const dispatch=useAppDispatch()

    const [canWrite,setCanWrite]=useState(false)
    const [stateTask,setStateTask]=useState(taskModel)

    return(
        <>
            <Card style={{
                margin:"1%",
                minWidth:"200px",
                maxWidth:"350px",
            }}>
                <CardContent>
                    <Grid container spacing={4} direction='column'>
                        <Grid item>
                            <ComponentShowOrWrite canWrite={canWrite} value={stateTask.title} 
                                label={t("title")} placeholder={t("pl.title")}
                                updateAction={(e)=>{setStateTask({...stateTask,title:e})}}/>
                        </Grid>
                        <Grid item>
                            <DatePicker
                                disabled={!canWrite}
                                mask="____/__/__"
                                value={taskModel.dateTime}
                                onChange={(newValue) => newValue&&setStateTask({...stateTask,dateTime:newValue})}
                                renderInput={(params) => <TextFieldToDatePicker {...params}/>}
                            />
                        </Grid>
                        <Grid item>
                            <ComponentShowOrWrite canWrite={canWrite} value={stateTask.description}
                                label={t("description")} placeholder={t("pl.description")}
                                updateAction={(e)=>{setStateTask({...stateTask,description:e})}}/>
                        </Grid>
                    </Grid>
                </CardContent>
                <CardActions>
                    {
                        !canWrite&&
                        <Button onClick={(e)=>{
                            setCanWrite(true)
                        }}>Edit Fields</Button>
                    }
                    {
                        canWrite&&
                        <Button onClick={(e)=>{
                            dispatch(updateTask(stateTask))
                            setCanWrite(false)
                        }}
                            >Update</Button>
                    }
                </CardActions>
            </Card>

        </>
    )
}

export default CardTask