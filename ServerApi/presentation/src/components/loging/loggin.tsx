import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router";
import { useAppDispatch } from "../../redux/store";
import { Button, Grid, TextField } from "@material-ui/core";
import { PathLocalHome } from "../../routes/local/localPaths";
import { loggingUser } from "../../redux/user/actionAuthentication";
import { getTask } from "../../redux/user/actionTask";



const Logging: React.FC<{}> = () => {
    const {t} = useTranslation()

    const [logging, setLogging] = useState({
        userName: "",
        password: "",
    });
    
    const dispatch=useAppDispatch()
    const navigator=useNavigate()
    
    return (
        
        <Grid container spacing={4}
            justifyContent="center" alignContent="center">
            <Grid xs={12} container item>
                <TextField label={t("username")} placeholder={t("pl.username")}
                    fullWidth required onChange={(e) => { setLogging({ ...logging, userName: e.target.value }) }} />
                
            </Grid>
            <Grid xs={12} container item>
                <TextField label={t("password")} placeholder={t("pl.password")}
                    fullWidth required onChange={(e) => { setLogging({ ...logging, password: e.target.value }) }}/>
            </Grid>
            <Grid xs={12} container item
                alignContent="center" justifyContent="center">
                <Button onClick={(e)=>{
                    dispatch(loggingUser(logging)).then(e=>{
                        dispatch(getTask())
                        navigator(PathLocalHome)
                    })}
                }> {t("logging")}</Button>
            </Grid>
        </Grid>
                
    )
}

export default Logging