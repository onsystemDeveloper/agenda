import React, { useState } from "react";
import { Button, Grid, TextField } from "@material-ui/core";
import { useTranslation } from "react-i18next";
import { useAppDispatch } from "../../redux/store";
import { registerUser } from "../../redux/user/actionAuthentication";



const Register:React.FC<{}>=()=>{
    const {t} =useTranslation()
    const dispatch=useAppDispatch()
    const [stateRegisterUser,setstateRegisterUser]=useState({
        name:"",
        userName:"",
        email:"",
        password:""
    })
    return (
        <Grid container spacing={4}>
            <Grid item xs={12}>
                <TextField label={t("name")} placeholder={t("pl.name")}
                    fullWidth required onChange={(e)=>{setstateRegisterUser({...stateRegisterUser,name:e.target.value})}}/>
            </Grid>
            <Grid item xs={12}>
                <TextField label={t("username")} placeholder={t("pl.username")}
                    fullWidth required onChange={(e)=>{setstateRegisterUser({...stateRegisterUser,userName:e.target.value})}}/>
            </Grid>
            <Grid item xs={12}>
                <TextField label={t("email")} placeholder={t("pl.email")}
                    fullWidth required onChange={(e)=>{setstateRegisterUser({...stateRegisterUser,email:e.target.value})}}/>
            </Grid>
            <Grid item xs={12}>
                <TextField label={t("password")} placeholder={t("pl.password")}
                    fullWidth required onChange={(e)=>{setstateRegisterUser({...stateRegisterUser,password:e.target.value})}}/>
            </Grid>
            <Grid container item xs={12} justifyContent="center">
                <Button onClick={(e)=>{
                    dispatch(registerUser(stateRegisterUser))
                }}>
                    {t("register")}
                </Button>
            </Grid>
        </Grid>
    )
}


export default Register