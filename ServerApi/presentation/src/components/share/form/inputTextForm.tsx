import { FormControl, FormLabel, TextField } from "@material-ui/core";
import React from "react";



const InputTextForm: React.FC<{
    id:string
    label?: string,
    valueInput: string,
    error: boolean,
    onChangeAction: (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void
}> = ({
    id,
    label,
    valueInput,
    error,
    onChangeAction
}) => {
        return (
            <FormControl>
                {
                    label &&
                    <FormLabel>{label}</FormLabel>

                }

                <TextField key={id} 
                    onChange={onChangeAction}
                    error={error}
                    value={valueInput} />


            </FormControl>
        )
    }


export default InputTextForm