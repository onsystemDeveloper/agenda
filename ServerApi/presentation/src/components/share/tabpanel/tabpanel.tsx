import React from "react";

export type TabPanelProps={
    children?: JSX.Element;
    index: number;
    value: number;
}

const TabPanel:React.FC<TabPanelProps>=({index,value,children})=>{
    return (
        <div role="tabpanel"
            hidden={value !== index}>
            {
                (value===index&&children!=null)&&(
                    <div>
                        {children}
                    </div>
                )
            }
        </div>
    )
}

export default TabPanel