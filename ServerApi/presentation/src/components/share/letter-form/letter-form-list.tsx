import { Button, List, ListItem } from "@material-ui/core";
import React from "react";

export type Field = {
    component: JSX.Element,
    justifyContent?:"center"|"flex-start"|"flex-end"
}


export type ConfigurationForm = {
    id: string,
    justifyContent?:"center"|"flex-start"|"flex-end"
    onSubmit: {
        nameLabel: string,
        enable: boolean | null | undefined,
        action: () => void
    },
    cancel?: {
        nameLabel:string,
        enable: boolean | null | undefined,
        action: () => void
    }
}

type Props = {
    fields: Array<Field>,
    configForm: ConfigurationForm
}

const LetterFormList: React.FC<Props> = ({ fields, configForm }) => {
    return (
        <List>
            {
                fields.map((element, index) => {
                    return (
                        <ListItem key={`letter-form-${configForm.id}-${index}`}
                        style={{
                            justifyContent:element.justifyContent?element.justifyContent:"center"
                        }}>
                           {element.component}
                        </ListItem>
                    )
                })
            }

            <ListItem 
                style={{
                    justifyContent:configForm.justifyContent?configForm.justifyContent:"center"
                }}>
                {
                    configForm.onSubmit.enable &&
                    <Button onClick={(e) => { configForm.onSubmit.action() }}>{configForm.onSubmit.nameLabel}</Button>
                }

                {
                    configForm.cancel && configForm.cancel.enable &&
                    <Button onClick={(e) => { configForm.cancel!.action() }}>{configForm.cancel!.nameLabel}</Button>
                }
            </ListItem>
        </List>

    )
}


export default LetterFormList;





