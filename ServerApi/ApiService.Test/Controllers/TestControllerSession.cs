using ApiService.Data.Dto;
using Microsoft.AspNetCore.Mvc.Testing;

namespace ApiService.Test.Controllers
{
    public class TestControllerSession
    {
        private WebApplicationFactory<Program> application;
        private HttpClient client;

        [SetUp]
        public void Setup()
        {
            application = new WebApplicationFactory<Program>();
            client = application.CreateDefaultClient();
        }

        [Test]
        public async Task BasicLogging()
        {

            LogginUserDto logging = new LogginUserDto()
            {
                UserName = "string",
                Password = "string"
            };
            HttpResponseMessage response = await client.PostAsJsonAsync("/logging", logging);
            int statusCode = (int)response.StatusCode;
            string? cookies = response.Headers.GetValues("Set-Cookie").FirstOrDefault();

            Assert.Multiple(() =>
            {
                Assert.That(statusCode, Is.EqualTo(200));
                Assert.IsNotNull(cookies);
            });
        }
    }
}